package controllers

import javax.inject.Inject
import play.api.Logger
import play.api.libs.json.JsValue
import service.{ApiAIService, DialogFlowService, FbService}
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.Future

class FbController @Inject()(cc: ControllerComponents, dfService: DialogFlowService, fbService:FbService)(implicit ec: ExecutionContext)  extends AbstractController(cc) {

  def verifyToken() = Action { implicit request: Request[AnyContent] =>
    val token = request.getQueryString("hub.verify_token").getOrElse("")
    val mode = request.getQueryString("hub.mode").getOrElse("")
    val challenge = request.getQueryString("hub.challenge").getOrElse("")
    if (fbService.verifyToken(token, mode, challenge))
      Ok(challenge)
    else
      BadRequest("not valid token")
  }

  def handleMessage = Action.async { implicit request: Request[AnyContent] =>
    val messageJson = request.body.asJson.get
    Logger.info(s"body::::${messageJson.toString()}")
    val senderId = ((messageJson \\ "sender")(0) \ "id").as[String]
    Logger.info(s"senderId: $senderId")
    val text = (messageJson \\ "text") (0).as[String]
    Logger.info(s"text: $text")
    dfService.detectIntent(text, "fb", senderId).map {
      x =>
        Logger.info(x.toString())
        Ok("")
    }

  }

  def sendDFEvent = Action {  implicit request: Request[AnyContent] =>
    val messageJson = request.body.asJson.get
    val sessionId = (messageJson \ "session-id").as[String]
    Ok(s"result of event call: ${dfService.sendAuthEvent(sessionId)}")
  }
}
