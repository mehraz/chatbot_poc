package controllers

import javax.inject.Inject

import dto.{AIRequest, Response, SlackRequest, SlackResponse}
import service.ApiAIService
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws._
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import scala.concurrent.{ExecutionContext, Future}

class SlackController @Inject()(cc: ControllerComponents, aiService: ApiAIService)(implicit ec: ExecutionContext)  extends AbstractController(cc) {

  def getMessage() = Action.async { implicit request: Request[AnyContent] =>

    val json = request.body.asJson.get
    //Logger.info(s"message is received: ${json}")
    val userQuery = (json \ "query").as[String]
    val sessionId = (json \ "sessionId").as[String]
    val query = AIRequest(contexts = List("greeting"),query=userQuery, sessionId = sessionId)
    val resp = aiService.dialog(query)

    for {
      response <- resp
    } yield {

      //val transformed = Response("success", response.json)
      Ok(response.json)
    }
  }

  def slack() = Action.async { implicit request: Request[AnyContent] =>

    val form = request.body.asFormUrlEncoded.get
    val sessionId = form.get("user_id").map(_.head).get
    if(sessionId!="USLACKBOT") {


      val text = form.get("text").map(_.head).get
      //Logger.info(s"message is received: ${json}")
      val query = AIRequest(contexts = List("greeting"), query = text, sessionId = sessionId)
      val resp = aiService.dialog(query)

      for {
        response <- resp
      } yield {

        //val transformed = Response("success", response.json)
        val slackResp = covertAIResponseToSlackResponse(response.json)
        Ok(SlackResponse.toJson(slackResp))
      }
    }else{
      Future(Ok("{}"))
    }
  }

  def covertAIResponseToSlackResponse(json:JsValue) = {
    val msg = (json \ "result" \ "fulfillment" \ "speech").get.as[String]
    SlackResponse(msg)
    //fulfillment speech
  }
}
