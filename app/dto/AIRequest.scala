package dto

import play.api.libs.json.{JsObject, JsValue, Json}

case class AIRequest(contexts: List[String],
                     lang: String = "en",
                     sessionId: String = "12345",
                     query: String,
                     timezone: String = "America/New_York"
                    )

object AIRequest {
  implicit val aiRequestReads = Json.reads[dto.AIRequest]
  implicit val aiRequestWrites = Json.writes[dto.AIRequest]

  def toJson(req: dto.AIRequest) = {
    Json.toJson(req)
  }
}