package dto

import play.api.libs.json.{JsObject, JsValue, Json}

case class Response(status: String, data: JsValue)

object Response {
  implicit val responseReads = Json.reads[dto.Response]
  implicit val responseWrites = Json.writes[dto.Response]

  def toJson(response: dto.Response) = {
    Json.toJson(response)
  }
}

