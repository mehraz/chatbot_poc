package dto

import play.api.libs.json.Json

case class SlackRequest(
  user_id:String="",
  user_name:String="",
  text:String=""
  //trigger_word:String=""
                       ) {

}

object SlackRequest {
  implicit val slackRequestReads = Json.reads[dto.SlackRequest]
  implicit val slackRequestWrites = Json.writes[dto.SlackRequest]

  def toJson(req: dto.SlackRequest) = {
    Json.toJson(req)
  }
}


/*


token:String="", team_id:String="",
  team_domain:String="",
  channel_id:String="",
  channel_name:String="",
  //thread_ts:String="",
  //timestamp:String="",

token=XXXXXXXXXXXXXXXXXX
team_id=T0001
team_domain=example
channel_id=C2147483705
channel_name=test
thread_ts=1504640714.003543
timestamp=1504640775.000005
user_id=U2147483697
user_name=Steve
text=googlebot: What is the air-speed velocity of an unladen swallow?
trigger_word=googlebot:
 */
