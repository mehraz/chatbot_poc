package dto

import play.api.libs.json.Json

case class SlackResponse(text:String) {

}

object SlackResponse {
  implicit val slackRespReads = Json.reads[dto.SlackResponse]
  implicit val slackRespWrites = Json.writes[dto.SlackResponse]

  def toJson(req: dto.SlackResponse) = {
    Json.toJson(req)
  }


}
