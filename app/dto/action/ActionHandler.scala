package dto.action

import play.api.Logger
import play.api.libs.json.JsValue

object ActionHandler {

  def matchAction(actionName:String, props:JsValue): Option[ActionTrait] = {

    Logger.info(s"in matchAction: received action: ${actionName}. props: ${props}")
    actionName match {
      case "pin.reset.verify" => Option(PinResetVerifyAction.create(actionName, props))
      case _ => None
    }
  }

  def getActionName(json:JsValue) = {
    (json \ "result" \ "action").get.as[String]
  }

  def getParameters(json:JsValue) = {
    (json \ "result" \ "parameters").get
  }
}
