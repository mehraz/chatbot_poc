package dto.action

import play.api.libs.json.JsValue

case class EmptyAction (name:String="NO_ACTION") extends ActionTrait


object EmptyAction {

  def create(name:String, param:JsValue) = {
    EmptyAction()
  }


}
