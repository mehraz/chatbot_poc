package dto.action

import play.api.libs.json.{JsValue, Json}

case class PinResetVerifyAction(name:String, cardNo:String, lastName:String) extends ActionTrait


object PinResetVerifyAction {

  def create(name:String, param:JsValue) = {
    val cardNo = (param \ "card_no").get.as[String]
    val lastName = (param \ "lastname").get.as[String]

    PinResetVerifyAction(name, cardNo, lastName)
  }


}
