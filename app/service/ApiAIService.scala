package service

import javax.inject.Inject

import dto.AIRequest
import dto.action.{ActionHandler, ActionTrait, PinResetVerifyAction}
import play.api.Logger
import play.api.libs.json.JsValue
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class ApiAIService @Inject()(ws: WSClient) {


  def postQuery(aiReq:AIRequest): Future[WSResponse] ={

    val req: WSRequest = ws.url("https://api.dialogflow.com/v1/query?v=20170712")
      .addHttpHeaders("Content-Type" -> "application/json")
      .addHttpHeaders("Authorization" -> "Bearer 949cc193b4924bbc826eea05cebb5511")
      //.addQueryStringParameters("v" -> "20170712")
      .withRequestTimeout(10000.millis)

    val queryJson = AIRequest.toJson(aiReq)
    Logger.info(s"query for AI: ${queryJson}")
    val res = req.post(queryJson)
    res
  }

  def dialog(aiReq:AIRequest): Future[WSResponse] = {

    Logger.info("----------------------------------------------------------------")

    val meaning: Future[WSResponse] = postQuery(aiReq)
    val action: Future[Option[ActionTrait]] = getAction(meaning)

    action.flatMap(x=> x match{
      case Some(actionValue) => {
        Logger.info(s"Action to take: ${actionValue}")
        val resultOfAction = performAction(actionValue)
        Logger.info(s"Result of Action: ${resultOfAction}")
        val newReq = AIRequest(aiReq.contexts, aiReq.lang, aiReq.sessionId, resultOfAction, aiReq.timezone)
        //Logger.info(s"newRequest is created:${newReq}")
        dialog(newReq)
      }
      case None => meaning
    })

  }

  def verifyCollectorForPinReset(cardNo:String, lastName:String):String = {
    "action_collector_verified_success firstname=Ross ."
  }

  def performAction(action:ActionTrait): String = {
    action match {
      case PinResetVerifyAction(name, cardNo, lastName) => verifyCollectorForPinReset(cardNo, lastName)
      case _ => "Nothing"
    }
  }


  def getAction(result:Future[WSResponse]) = {
    val json: Future[JsValue] = result.map(x => {
      Logger.info(s"received data: ${x.json}")
      x.json})

    val action = for{
      jsonVal <-json

      name = ActionHandler.getActionName(jsonVal)
      props = ActionHandler.getParameters(jsonVal)
    } yield (ActionHandler.matchAction(name, props))

    action

  }
}
