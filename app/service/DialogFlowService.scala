package service

import javax.inject.Inject
import play.api.libs.json.{JsNull, JsString, JsValue, Json}
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class DialogFlowService @Inject()(ws: WSClient, dfFacade:DialogflowFacade) {

  val agentId = "mehdi-d8aab"

  def detectIntent(msg: String, channel: String, senderId: String, lang:String = "en-CA"): Future[JsValue] = {

    ws.url(s"https://dialogflow.googleapis.com/v2/projects/$agentId/agent/sessions/${channel}_$senderId:detectIntent")
      .addHttpHeaders("Content-Type" -> "application/json")
      .addHttpHeaders("charset" -> "utf-8")
      .addHttpHeaders("Authorization" -> "Bearer ya29.GlvyBdhUGjqZiYEF2J2F_FJfH3GQZYuPRvDBsOE4p45MtgF0Gt6ZseRShM6MnlejspEuRFMvomVMtHPYUkd0zu8v71ADOEABmfV-Oo0DfcYm8TPhZ_ADkNWO2XCt")
      .withRequestTimeout(10000.millis)
      .post(
        Json.obj(
          "queryInput" -> Json.obj(
            "text" -> Json.obj(
              "text" -> msg,
              "languageCode" -> lang)
          ),
          "queryParams" -> Json.obj("timeZone" -> "America/Toronto")
        )
      ).map(_.json)
  }

  def sendAuthEvent(sessionId:String) = {
    dfFacade.sendEvent(sessionId, "auth_process_complete", Map[String, String]("code"-> "yaay"))
  }

  def getMessages(jsValue: JsValue): JsValue = {
    (jsValue \\ "fulfillmentMessages")(0)
  }
}

