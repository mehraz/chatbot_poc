package service

import com.google.cloud.dialogflow.v2._
import com.google.protobuf.{Struct, Value}
import play.api.Logger

import collection.JavaConverters._

class DialogflowFacade {


  private lazy val sessionClient = SessionsClient.create()

  def sendEvent(sessionId:String, eventName:String, params:Map[String, String] ) = {

    val queryInput = QueryInput.newBuilder().setEvent(
      EventInput.newBuilder()
        .setLanguageCode("en-CA")
        .setName("auth_process_complete")
        .setParameters(Struct.newBuilder().putAllFields(params.mapValues(x=>Value.newBuilder().setStringValue(x).build()).asJava))
        .build()
    ).build()

    val sessionName = SessionName.newBuilder()
      .setProject("enzo-s-auth-bot")
      .setSession(sessionId)
      .build()

    val res = sessionClient.detectIntent(sessionName, queryInput)
    Logger.info(s"res: ${res.toString}")
    res.hasQueryResult
  }


}
