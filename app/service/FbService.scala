package service

import javax.inject.Inject
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient
import play.api.{Configuration, Logger}
import com.google.cloud.dialogflow.v2._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class FbService @Inject()(config: Configuration, ws: WSClient) {

  lazy val verfyToken = config.get[String]("fb.verify-token")
  private lazy val pageAccessToken = config.get[String]("fb.page-access-token")
  def verifyToken(token: String, mode: String, challenge: String) = {

    if(mode == "subscribe" && token == verfyToken) {
      Logger.info(s"Verify webhook token: ${token}, mode ${mode}")
      true
    }
    else {
      Logger.error(s"Invalid webhook token: ${token} != $verfyToken. mode ${mode}")
      false
    }
  }

  def sendMessage(recepientId:String, msgs: JsValue, lang:String = "en-CA") = {

    ws.url(s"https://graph.facebook.com/v2.6/me/messages?access_token=$pageAccessToken")
      .addHttpHeaders("Content-Type" -> "application/json")
      .addHttpHeaders("charset" -> "utf-8")
      .withRequestTimeout(10000.millis)
      .post(
        Json.obj(
          "queryInput" -> Json.obj(
            "text" -> Json.obj(
              "text" -> msgs,
              "languageCode" -> lang)
          ),
          "queryParams" -> Json.obj("timeZone" -> "America/Toronto")
        )
      ).map(_.json)
  }
}
