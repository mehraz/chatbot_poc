import sbt.util

lazy val commonSettings = Seq(
  organization := "com.loyalty",
  scalaVersion := "2.12.3",
  version := "1.0-SNAPSHOT",
  name := "email-acquisition-api"
)


lazy val IntegrationTest = config("it") extend (Test)

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(Defaults.itSettings: _*)
  .settings(
    name := "email-acquisition-api",
    logLevel := util.Level.Info,
    commonSettings,
    libraryDependencies ++= Seq(
      guice,
      ws,
      ehcache,
      "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % "test,it",
      "org.mockito" % "mockito-core" % "2.10.0" % "test",
      "org.scalatest" %% "scalatest" % "3.0.4" % "test",
      "io.getquill" %% "quill-async-mysql" % "2.0.0",
      "com.typesafe.play" %% "play" % "2.6.7" intransitive(),
      "com.google.cloud" % "google-cloud-dialogflow" % "0.54.0-alpha",
      "com.google.auth" % "google-auth-library-oauth2-http" % "0.10.0"
    ),

    //fork in IntegrationTest := true,
    sourceDirectory in IntegrationTest := baseDirectory.value / "it",
    scalaSource in IntegrationTest := baseDirectory.value / "it",
    coverageExcludedPackages :="""controllers.javascript;controllers\..*Reverse.*;router.Routes.*;views.*;configuration""",
    coverageEnabled in(Test, compile) := true,
    coverageEnabled in(IntegrationTest, compile) := false,
    coverageEnabled in(Compile, compile) := false,
    coverageMinimum := 60,
    coverageFailOnMinimum := false

  ).enablePlugins(PlayScala)